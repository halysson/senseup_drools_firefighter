# Firefighter learning project

This project named *Firefighter* is a Drools project that incentivates the basic learn about it and to try to solve a loop problem.

You will find this learn challenge description in this [link](https://www.slideshare.net/salaboy/drools5-community-training-module-15-drools-expert-first-example).

## To download this project

With this url: https://gitlab.com/halysson/senseup_drools_firefighter.git

On the Eclipse IDE > File > Importi > Git > Projects from Git > clone URI

## Update the project

When the project is on the Eclipse, click in the project root folder with right button of the mouse > Maven > Update project

## Running the project

The App.java is the class with main method. Click in with right button and run as Java application.

