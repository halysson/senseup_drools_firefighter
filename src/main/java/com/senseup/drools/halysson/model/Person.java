package com.senseup.drools.halysson.model;

public class Person {
    private String name;
    private Pet pet;
    private int petCallCount = 0;

	public Person() {
    }

    public Person(String name) {
    	System.out.println(name +"(Person)");
        this.name = name;
    }


    public String getName() {
        return name;
    }

    

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public int getPetCallCount() {
        return petCallCount;
    }

    public void setPetCallCount(int petCallCount) {
        this.petCallCount = petCallCount;
    }

    

    @Override
    public String toString() {
        return "Person[name= '"+name+"', petName= '"+pet.getName()+"', petCallCount= '"+petCallCount+"']";
    }
    
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((pet == null) ? 0 : pet.hashCode());
		result = prime * result + petCallCount;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (pet == null) {
			if (other.pet != null)
				return false;
		} else if (!pet.equals(other.pet))
			return false;
		if (petCallCount != other.petCallCount)
			return false;
		return true;
	}
    
}
