package com.senseup.drools.halysson;

import org.junit.Assert;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import com.senseup.drools.halysson.model.Person;
import com.senseup.drools.halysson.model.Pet;

public class App {

	public static void main(String[] args) {
		
		System.out.println("Bootstrapping the Rule Engine ...");
		// Bootstrapping a Rule Engine Session
		KieServices ks = KieServices.Factory.get();
		KieContainer kContainer = ks.getKieClasspathContainer();
		KieSession kSession = kContainer.newKieSession();
		
		if (kSession == null) System.out.println("kSession Null");

		// Create a Person
        Person person = new Person("Salaboy!");
        // Create a Pet
        Pet cat = new Pet("mittens", "on a limb", Pet.PetType.CAT);
        // Set the Pet to the Person
        person.setPet(cat);

        // Now we will insert the Pet and the Person into the KnowledgeSession
        kSession.insert(cat);
        kSession.insert(person);
		int fired = kSession.fireAllRules();
		System.out.println("Number of Rules executed = " + fired);
		
		Assert.assertEquals(cat.getPosition(), "on the street");
		
		//Dispose the knowledge session
		kSession.dispose();
		
	}
}
